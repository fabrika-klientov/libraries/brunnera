<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Brunnera
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera;


use Brunnera\Core\Helpers\Instances;
use Brunnera\Core\Query\HttpClient;

class Client
{
    use Instances;

    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param array $auth ['domain' => string, 'apiKey' => string] (optional)
     * @return void
     * */
    public function __construct(array $auth = null)
    {
        if ($this->validateAuth($auth) && !empty($auth)) {
            $this->httpClient = new HttpClient($auth);
        }
    }

    /**
     * @return HttpClient|null
     * */
    public function getHttpClient()
    {
        return $this->httpClient ?? null;
    }

    /** client can request data
     * @return bool
     * */
    public function isActiveClient()
    {
        return isset($this->httpClient);
    }

    /** validator for auth data
     * @param array $auth
     * @return bool
     * */
    protected function validateAuth(array $auth = null)
    {
        if (empty($auth)) {
            return true;
        }

        if (isset($auth['domain'], $auth['apiKey']) && is_string($auth['domain']) && is_string($auth['apiKey'])) {
            return true;
        }

        return false;
    }
}