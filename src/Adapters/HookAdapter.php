<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Adapters;


use Brunnera\Client;
use Brunnera\Core\Helpers\Base;
use Brunnera\Models\Calls;

/**
 * @property string $requestType
 * */
class HookAdapter extends Base
{
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @var string[] $supportedRequestTypes
     * */
    protected $supportedRequestTypes = [
        'call.dial',
        'call.bridge',
        'call.hangup',
    ];

    /**
     * @var Calls|null $fullData
     * */
    protected $fullData;

    /**
     * @var array $utm_fields
     * */
    protected $utm_fields = [
        'utmSource' => 'utm_source',
        'utmMedium' => 'utm_medium',
        'utmCampaign' => 'utm_campaign',
        'utmContent' => 'utm_content',
        'utmTerm' => 'utm_term',
        'clientIp' => 'ipAddress',
        'referer' => 'domain',
    ];

    /** constructor and init hook data
     * @param array $data
     * @param Client $client
     * @return void
     * */
    public function __construct(array $data = [], $client = null)
    {
        parent::__construct($data);
        $this->validate();
        $this->client = $client;
    }


    /**
     * @override
     * @return string
     * */
    public function getRequestType()
    {
        return $this->data['event'];
    }

    /**
     * @override
     * @return array
     * */
    public function getTags()
    {
        if ($this->isCallTracking()) {
            return ['callTracking'];
        }

        if ($this->isGetCall()) {
            return ['getCall'];
        }

        return [];
    }

    /**
     * @override
     * @return bool
     * */
    public function isCallTracking()
    {
        if (!empty($this->data['label'])) {
            return strpos($this->data['label'], 'call-tracker') !== false
                || strpos($this->data['label'], 'calltracker') !== false;
        }

        return false;
    }

    /**
     * @override
     * @return bool
     * */
    public function isGetCall()
    {
        if (!empty($this->data['label'])) {
            return strpos($this->data['label'], 'call-catcher') !== false ||
                strpos($this->data['label'], 'callcatcher') !== false;
        }

        return false;
    }

    /**
     * @override
     * @return string|null
     * */
    public function getExternalNumber()
    {
        if (isset($this->data['otherLegs'][0])) {
            return $this->data['otherLegs'][0]['num'];
        } elseif (!empty($this->data['otherLegNum'])) {
            return $this->data['otherLegNum'];
        }

        return null;
    }

    /**
     * @override
     * @return string|null
     * */
    public function getInternalNumber()
    {
        return $this->data['leg']['ext']
            ?? $this->data['trunkSource']
            ?? $this->getSourceNumber();
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSourceNumber()
    {
        return $this->data['trunkNum'] ?? $this->data['trunkSource'] ?? null;
    }

    /**
     * @override
     * @param string|null $key
     * @return array|null
     * */
    public function getTrackingData(string $key = null)
    {
        return $this->getUtmData($key == 'getTrackingData' ? null : $key);
    }

    /**
     * @param string|null $key
     * @return array|null
     * */
    protected function getUtmData(string $key = null)
    {
        $result = [];
        $data = array_intersect_key($this->data, $this->utm_fields);

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $result[$this->utm_fields[$key]] = $value;
            }
        }

        return isset($key) ? ($result[$key] ?? null) : $result;
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSourceName()
    {
        return !empty($this->data['trunkName']) ? $this->data['trunkName'] : null;
    }

    /**
     * @override
     * @param string|null $key
     * @return array|null
     * */
    public function getCallData(string $key = null)
    {
        return $this->getUtmData($key == 'getCallData' ? null : $key);
    }

    /**
     * @override
     * @return string
     * */
    public function getDisposition()
    {
        return (!empty($this->data['bridgeAt']) || !empty($this->data['callParentUuid']))
            ? 'ANSWER'
            : (isset($this->data['bridgeAt']) ? 'BUSY' : '');
    }

    /**
     * @override
     * @return string
     * */
    public function getDisplayName()
    {
        return $this->data['leg']['displayName'] ?? '';
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSite()
    {
        return $this->data['referer'] ?? null;
    }

    /**
     * @override
     * @return string|int|null
     * */
    public function getId()
    {
        return $this->data['uuid'] ?? $this->data['callParentUuid'] ?? null;
    }

    /**
     * @override
     * @return string|int|null
     * */
    public function getCompanyId()
    {
        return 'phonet';
    }

    /**
     * @override
     * @return bool
     * */
    public function isIncoming()
    {
        return (!empty($this->data['lgDirection']) && $this->data['lgDirection'] == 4) || $this->isCallTracking();
    }

    /**
     * @override
     * @return bool
     * */
    public function isAnswered()
    {
        return !empty($this->data['bridgeAt']) || !empty($this->data['callParentUuid']);
    }

    /**
     * @override
     * @return bool
     * */
    public function isCompleted()
    {
        return !empty($this->data['bridgeAt']) || !empty($this->data['callDate']);
    }

    /**
     * @override
     * @return string|int
     * */
    public function getStartTime()
    {
        return $this->data['bridgeAt'] ?? $this->data['callStart'] ?? time();
    }

    /**
     * @override
     * @return string|int|float
     * */
    public function getDuration()
    {
        if (is_null($this->data['bridgeAt'])) {
            return 0;
        }

        $tm_diff = ($this->data['serverTime'] ?? 0) - ($this->data['bridgeAt'] ?? 0);
        return $tm_diff > 0 ? $tm_diff / 1000 : 0;
    }

    /**
     * @override
     * @return string
     * */
    public function getRecordLink()
    {
        if (isset($this->client)) {
            $httpClient = $this->client->getHttpClient();
        }
        return !empty($this->data['uuid']) && isset($httpClient)
            ? 'https://' . ($httpClient->getAuth()['domain'] ?? '') . '/rest/public/calls/' . $this->data['uuid'] . '/audio'
            : '';
    }

    /**
     * @override
     * @throws \Exception
     * @return void
     * */
    public function validate()
    {
        if (empty($this->data) || !is_array($this->data)) {
            throw new \Exception('Data is empty or not array');
        }

        $data = $this->data;

        if (empty($data['event'])) {
            throw new \Exception('[event] is NULL');
        }

        if (!in_array($data['event'], $this->supportedRequestTypes)) {
            throw new \Exception('[event] ' . $data['event'] . ' not supported');
        }
    }
}