<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Models;


use Brunnera\Core\Build\Builder;
use Brunnera\Core\Helpers\Base;
use Brunnera\Core\Query\HttpClient;
use Illuminate\Support\Str;

abstract class Model extends Base
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;

    /**
     * @var Builder $builder
     * */
    protected $builder;

    /**
     * @var array $methods
     * */
    protected $methods = [];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod;

    /**
     * @var string $base
     * */
    protected $base = '/rest/';

    /**
     * @param HttpClient $client
     * @param array $data
     * @return void
     * */
    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($data);
        $this->httpClient = $client;
        $this->builder = new Builder();
    }

    /** get active method
     * @return string
     * */
    protected function getMethod(): string
    {
        return $this->currentMethod;
    }

    /** small entry partition
     * @return string
     * */
    protected function getEntry()
    {
        return Str::lower(last(explode('\\', get_class($this))));
    }

    /** get merged link
     * @return string
     * */
    protected function getLink()
    {
        return $this->base . $this->getEntry() . $this->getMethod();
    }

    /** magic method
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'timeFrom':
            case 'timeTo':
            case 'directions':
            case 'limit':
            case 'offset':
                $this->builder->{$name}(...$arguments);
                return $this;
        }

        return null;
    }

    /** mass inject in builder
     * @param array $params
     * @param array $scheme
     * @return void
     * */
    protected function massBuilder(array $params, array $scheme)
    {
        if (empty($params)) {
            return;
        }

        array_map(function ($schema, $value) {
            if (isset($value)) {
                $this->builder->{$schema}($value);
            }
            return null;
        }, $scheme, $params);
    }
}
