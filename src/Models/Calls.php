<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Models;

use Brunnera\Core\Collection\Collection;

/**
 * @method $this timeFrom($timeFrom)
 * @method $this timeTo($timeTo)
 * @method $this directions($directions)
 * @method $this limit($limit)
 * @method $this offset($offset)
 * */
final class Calls extends Model
{

    /**
     * @param string|int $timeFrom (required or before used timeFrom($timeFrom) method)
     * @param string|int $timeTo (required or before used timeTo($timeTo) method)
     * @param int $directions (1 — внутренние; 2 — исходящие; 4 — входящие; 32 — установка на паузу (нет на месте); 64 — снятие с паузы (есть на месте))
     * @param int $limit (max 50)
     * @param int $offset
     * @return Collection
     * */
    public function companyApiGet($timeFrom = null, $timeTo = null, $directions = null, $limit = null, $offset = null)
    {
        $this->currentMethod = '/company.api';
        $this->massBuilder(func_get_args(), ['timeFrom', 'timeTo', 'directions', 'limit', 'offset']);

        $result = $this->httpClient->get($this->getLink(), ['query' => $this->builder->getResult()]);

        $this->builder->clear();

        return new Collection(array_map(function ($item) {
            return new static($this->httpClient, $item);
        }, $result ?? []));
    }

    /** store new call (in test)
     * @param string $legExt
     * @param string $otherLegNum
     * @return string (uuid of call)
     * @throws \Exception
     * */
    public function makeCall($legExt, $otherLegNum)
    {
        $this->currentMethod = 'user/makeCall';
        $this->massBuilder(func_get_args(), ['legExt', 'otherLegNum']);

        $result = $this->httpClient->post($this->base . $this->getMethod(), ['json' => $this->builder->getResult()]);

        $this->builder->clear();

        return $result['uuid'] ?? null;
    }

}