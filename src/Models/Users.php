<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Models;

use Brunnera\Core\Collection\Collection;

final class Users extends Model
{

    /**
     * @return Collection
     * */
    public function get()
    {
        $this->currentMethod = '';
        $result = $this->httpClient->get($this->getLink());

        return new Collection(array_map(function ($item) {
            return new static($this->httpClient, $item);
        }, $result ?? []));
    }

}