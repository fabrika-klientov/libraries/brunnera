<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Core
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Core\Collection;


use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{

}