<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Core
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Core\Query;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Exception\ClientException;

class HttpClient
{
    /**
     * @var Client $client
     * */
    private $client;

    /**
     * @var array $authData
     * */
    private $authData;

    /**
     * @var array $sessionData
     * */
    private $sessionData = [];

    /**
     * @var string $LINK
     * */
    private static $LINK;

    /**
     * @param array $auth
     * @return void
     * */
    public function __construct(array $auth)
    {
        $this->authData = $auth;
        self::$LINK = 'https://' . $auth['domain'] . '/';
        $pathToCookie = bAppPath() . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'cookies' . DIRECTORY_SEPARATOR . 'phonet' . DIRECTORY_SEPARATOR;
        $this->issetAndStoreCookiesDir($pathToCookie);
        $this->sessionData['cookies'] = new FileCookieJar($pathToCookie . $auth['domain'] . '.json', true);

        $this->client = new Client([
            'base_uri' => self::$LINK,
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /** api GET
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function get($link, $options = [])
    {
        return $this->request('GET', $link, $options);
    }

    /** api POST
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function post($link, $options)
    {
        return $this->request('POST', $link, $options);
    }

    /** auth data
     * @return array
     * */
    public function getAuth()
    {
        return $this->authData;
    }

    /** full url
     * @param string $link
     * @return string
     * */
    public function getURL(string $link = '')
    {
        return self::$LINK . $link;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param bool $isForce
     * @param bool $withSessionData
     * @return array|null
     * @throws \Exception
     * */
    protected function request(string $method, string $link, array $options, bool $withSessionData = true, bool $isForce = false)
    {
        try {
            if ($withSessionData) {
                $options = $this->injectSessionData($options, $isForce);
            }
            $result = json_decode($this->client->request($method, $link, $options)->getBody()->getContents(), true);

            if (!empty($result)) {
                return $result;
            }
        } catch (ClientException $clientException) {
            if (($clientException->getCode() == 401 || $clientException->getCode() == 403) && !$isForce) {
                return $this->request($method, $link, $options, $withSessionData, true);
            }
            throw new \Exception('Request returned error. ' . $clientException->getMessage(), $clientException->getCode());
        } catch (\Exception $exception) {
            throw new \Exception('Request returned error. ' . $exception->getMessage());
        }

        return null;
    }

    /**
     * @param array $options
     * @param bool $force
     * @return array
     * @throws \Exception
     * */
    protected function injectSessionData(array $options, bool $force = false)
    {
        return array_replace_recursive($options, $this->auth($force)) ?? [];
    }

    /** auth
     * @param bool $force
     * @return array
     * @throws \Exception
     * */
    protected function auth(bool $force = false)
    {
        if ($force || $this->sessionData['cookies']->count() == 0) {
            $this->request('POST', '/rest/security/authorize', [
                'json' => $this->authData,
                'cookies' => $this->sessionData['cookies'],
            ], false, true);
        }

        return $this->sessionData;
    }

    /**
     * @param string $path
     * @throws \RuntimeException
     * @return void
     * */
    protected function issetAndStoreCookiesDir(string $path)
    {
        if (!file_exists($path)) {
            if (!mkdir($path, 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for cookies. Permission denied.');
            }
        }
    }
}