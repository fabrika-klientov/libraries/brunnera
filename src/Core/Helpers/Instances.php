<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Core
 * @category  Brunnera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Brunnera\Core\Helpers;


use Brunnera\Adapters\HookAdapter;
use Illuminate\Support\Str;

/**
 * @property-read \Brunnera\Models\Users $users
 * @property-read \Brunnera\Models\Calls $calls
 *
 * @method  \Brunnera\Adapters\HookAdapter hookAdapter(array $data)
 * */
trait Instances
{

    /** getter magic instances
     * @param string $name
     * @return \Brunnera\Models\Model|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'users':
            case 'calls':
                $class = 'Brunnera\\Models\\' . Str::ucfirst($name);
                return new $class($this->httpClient);
        }

        return null;
    }

    /** call magic instances
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'hookAdapter':
                return new HookAdapter(head($arguments), $this);
        }

        return null;
    }
}