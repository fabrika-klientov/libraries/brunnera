## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/brunnera/v/stable)](https://packagist.org/packages/shadoll/brunnera)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/brunnera/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/brunnera/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/brunnera/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/brunnera/commits/master)
[![License](https://poser.pugx.org/shadoll/brunnera/license)](https://packagist.org/packages/shadoll/brunnera)

[Phonet.ua](https://phonet.ua/) telephony PHP API/hook library

---


## Install

`composer require shadoll/brunnera`

---


## Использование

Библиотека в процессе разработки, заложен основной механизм (авторизация, клиент, Http клиент,
несколько моделей (которые будут пополнятся))

Клиент **\Brunnera\Client**

Первое что необходимо - создать клиент, от от которого вы сможете делать любые манипуляции

```php
$client = new \Brunnera\Client([
    'domain' => 'jarvis.phonet.com.ua',
    'apiKey' => 'LVyRf2dskcmkdfvmkdfmvkvumafhJNS5p',
]);

$httpClient = $client->getHttpClient();
```

Все методы моделей которые могут вернуть более одной сущности будут помещать их в коллекции 
`Brunnera\Core\Collection\Collection` которая в свою очередь наследуемая от 
`Illuminate\Support\Collection` и соответственно имеет широкий выбор различных методов для работы

**Модель `Brunnera\Models\Users`** Пользователи

Для работы некоторых возможностей, необходима информация о пользователях (сотрудниках компании).

- `get` Получение списка пользователей

```php
/**
 * @var \Brunnera\Core\Collection\Collection<\Brunnera\Models\Users> $collect
 */
$collect = $client->users->get();

```

**Модель `Brunnera\Models\Calls`** Звонки

- `companyApiGet` Получение списка совершенных звонков компании

```php
/**
 * @var \Brunnera\Core\Collection\Collection<\Brunnera\Models\Calls> $collect
 */
$collect = $client->calls->companyApiGet(1575488444000, 1575988444000);

// или 
$instance = $client->calls;

$instance->timeFrom(12345)
    ->timeTo(234566)
    ->directions(1);
$instance->limit(25);
$instance->offset(505);

/**
 * @var \Brunnera\Core\Collection\Collection<\Brunnera\Models\Calls> $collect
 */
$collect = $instance->companyApiGet();

```

- `makeCall` Выполнение звонка

```php
/**
 * @var string $uuid
 */
$uuid = $client->calls->makeCall('201', '+380988888888');

```


---

### Работа с Hooks

Для выборки разных данных с веб Hooks есть `\Brunnera\Adapters\HookAdapter` в котором 
достаточное количество методов для выборки. Необходимо лишь передать данные с хука.

```php

$adapter = $client->hookAdapter($arrayOfHooks);
// или без клиента
$adapter = new \Brunnera\Adapters\HookAdapter($arrayOfHooks);

echo $adapter->getInternalNumber();
echo $adapter->getTrackingData();
// ...

```

