<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:09
 */

namespace Tests\Models;

use Brunnera\Client;
use Brunnera\Models\Calls;
use Brunnera\Models\Model;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $model = new Calls($client->getHttpClient());

        $this->assertInstanceOf(Model::class, $model);
        $this->assertInstanceOf(Calls::class, $model);

        $model = new Calls($client->getHttpClient(), ['any' => 'data']);

        $this->assertEquals('data', $model->any);
    }

    public function test__call()
    {
        // magic
        $this->assertTrue(true);
    }
}
