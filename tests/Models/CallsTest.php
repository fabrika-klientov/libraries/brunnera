<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:57
 */

namespace Tests\Models;

use Brunnera\Client;
use Brunnera\Models\Calls;
use PHPUnit\Framework\TestCase;

class CallsTest extends TestCase
{
    /**
     * @var Calls $model
     * */
    protected $model;

    public function testCompanyApiGet()
    {
        // request
        $this->assertTrue(true);
    }

    public function testMakeCall()
    {
        // request
        $this->assertTrue(true);
    }

    public function testTimeFrom()
    {
        $this->assertInstanceOf(Calls::class, $this->model->timeFrom(123456));
    }

    public function testTimeTo()
    {
        $this->assertInstanceOf(Calls::class, $this->model->timeTo(123456));
    }

    public function testDirections()
    {
        $this->assertInstanceOf(Calls::class, $this->model->directions(1));
    }

    public function testLimit()
    {
        $this->assertInstanceOf(Calls::class, $this->model->limit(50));
    }

    public function testOffset()
    {
        $this->assertInstanceOf(Calls::class, $this->model->offset(135));
    }

    protected function setUp(): void
    {
        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);
        $this->model = new Calls($client->getHttpClient());
    }
}
