<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 09.12.19
 * Time: 18:03
 */

namespace Tests;

use Brunnera\Client;
use Brunnera\Core\Query\HttpClient;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client();
        $this->assertInstanceOf(Client::class, $client);

        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testGetHttpClient()
    {

        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $this->assertInstanceOf(HttpClient::class, $client->getHttpClient());

        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
        ]);

        $this->assertNull($client->getHttpClient());

        $client = new Client([
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $this->assertNull($client->getHttpClient());

        $client = new Client([]);

        $this->assertNull($client->getHttpClient());

        $client = new Client();

        $this->assertNull($client->getHttpClient());
    }
}
