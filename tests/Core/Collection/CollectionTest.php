<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:21
 */

namespace Tests\Core\Collection;

use Brunnera\Client;
use Brunnera\Core\Collection\Collection;
use Brunnera\Models\Calls;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $collect = new Collection(array_map(function ($one) use ($client) {
            return new Calls($client->getHttpClient(), ['id' => $one]);
        }, [1, 2, 3, 4, 5]));

        $this->assertInstanceOf(Collection::class, $collect);

        $this->assertCount(5, $collect);

        $this->assertLessThan(6, $collect->shuffle()->first()->id);
    }
}
