<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:16
 */

namespace Tests\Core\Query;

use Brunnera\Core\Query\HttpClient;
use PHPUnit\Framework\TestCase;

class HttpClientTest extends TestCase
{

    /**
     * @var HttpClient $client
     * */
    protected $client;

    public function test__construct()
    {
        $client = new HttpClient([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $this->assertInstanceOf(HttpClient::class, $client);

    }

    public function testPost()
    {
        // is request
        $this->assertTrue(true);
    }

    public function testGetAuth()
    {
        // is request
        $this->assertTrue(true);
    }

    public function testGetURL()
    {
        $this->assertEquals('https://jarvis.phonet.com.ua/', $this->client->getURL());
        $this->assertEquals('https://jarvis.phonet.com.ua/entry', $this->client->getURL('entry'));
    }

    protected function setUp(): void
    {
        $this->client = new HttpClient([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);
    }
}
