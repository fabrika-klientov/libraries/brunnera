<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:27
 */

namespace Tests\Core\Build;

use Brunnera\Core\Build\Builder;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{

    /**
     * @var Builder $builder
     * */
    protected $builder;

    public function test__construct()
    {
        $builder = new Builder();

        $this->assertInstanceOf(Builder::class, $builder);
    }

    public function testWhere()
    {
        $this->builder->where('key', 'value');
        $this->assertEquals(['key' => 'value'], $this->builder->getResult());

        $this->assertInstanceOf(Builder::class, $this->builder->where('key', 'value'));
    }

    public function testClear()
    {
        $this->builder->where('key', 'value');
        $this->assertEquals(['key' => 'value'], $this->builder->getResult());

        $this->builder->clear();

        $this->assertEmpty($this->builder->getResult());
    }

    public function testGetResult()
    {
        $this->builder->where('key', 'value');
        $this->assertEquals(['key' => 'value'], $this->builder->getResult());

        $this->builder->clear();

        $this->assertEmpty($this->builder->getResult());
    }

    public function testHas()
    {
        $this->builder->where('key', 'value');

        $this->assertTrue($this->builder->has('key'));
        $this->assertFalse($this->builder->has('no_key'));
    }

    public function test__call()
    {
        $this->builder->key('value');
        $this->assertEquals(['key' => 'value'], $this->builder->getResult());

        $this->builder->clear();

        $this->assertEmpty($this->builder->getResult());
    }

    protected function setUp(): void
    {
        $this->builder = new Builder();
    }

    protected function tearDown(): void
    {
        $this->builder = null;
    }
}
