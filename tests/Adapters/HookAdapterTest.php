<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 11:46
 */

namespace Tests\Adapters;

use Brunnera\Adapters\HookAdapter;
use Brunnera\Client;
use PHPUnit\Framework\TestCase;

class HookAdapterTest extends TestCase
{
    /**
     * @var HookAdapter $adapter
     * */
    protected $adapter;
    protected $hook = '{"hash":"f3eb94ac73dd36dc8cef01586406fcdb","accountDomain":"igb.phonet.com.ua","event":"call.hangup","id":"68-1575992458.7861","uuid":"7b239623-c721-4725-8418-4503bdab372a","parentUuid":"7b239623-c721-4725-8418-4503bdab372a","lgDirection":2,"leg":{"id":4762,"type":1,"displayName":"Скляров Олександр - Нивки","ext":"163"},"leg2":null,"otherLegs":[{"id":null,"name":null,"num":"+380988255187","companyName":null,"url":"https://igb.phonet.com.ua/features/crm/contacts/edit/?searchPhone=%2B380988255187","priority":5,"connectLegIds":[]},{"id":null,"name":null,"num":"+380988255187","companyName":null,"url":null,"priority":0,"connectLegIds":[]}],"trunkNum":null,"trunkName":null,"dialAt":1575992461528,"bridgeAt":1575992482014,"transferHistory":"163","serverTime":1575992690801}';

    public function test__construct()
    {
        $adapter = new HookAdapter(json_decode($this->hook, true));

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $client = new Client([
            'domain' => 'jarvis.phonet.com.ua',
            'apiKey' => 'LVyRf2FVNeIrjhvffgumafhJNS5p',
        ]);

        $adapter = $client->hookAdapter(json_decode($this->hook, true));

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $adapter = new HookAdapter(json_decode($this->hook, true), $client);

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $hook = json_decode($this->hook, true);

        unset($hook['event']);

        $this->expectException(\Exception::class);

        new HookAdapter($hook);
    }

    public function testGetRequestType()
    {
        $this->assertEquals('call.hangup', $this->adapter->getRequestType());
    }

    public function testGetTags()
    {
        $this->assertEmpty($this->adapter->getTags());
    }

    public function testIsCallTracking()
    {
        $this->assertFalse($this->adapter->isCallTracking());
    }

    public function testIsGetCall()
    {
        $this->assertFalse($this->adapter->isGetCall());
    }

    public function testGetExternalNumber()
    {
        $this->assertEquals('+380988255187', $this->adapter->getExternalNumber());
    }

    public function testGetInternalNumber()
    {
        $this->assertEquals('163', $this->adapter->getInternalNumber());
    }

    public function testGetSourceNumber()
    {
        $this->assertNull($this->adapter->getSourceNumber());
    }

    public function testGetTrackingData()
    {
        $this->assertEmpty($this->adapter->getTrackingData());
    }

    public function testGetSourceName()
    {
        $this->assertNull($this->adapter->getSourceName());
    }

    public function testGetCallData()
    {
        $this->assertEmpty($this->adapter->getCallData());
    }

    public function testGetDisposition()
    {
        $this->assertEquals('ANSWER', $this->adapter->getDisposition());
    }

    public function testGetDisplayName()
    {
        $this->assertIsString($this->adapter->getDisplayName());
        $this->assertEquals('Скляров Олександр - Нивки', $this->adapter->getDisplayName());
    }

    public function testGetSite()
    {
        $this->assertNull($this->adapter->getSite());
    }

    public function testGetId()
    {
        $this->assertEquals('7b239623-c721-4725-8418-4503bdab372a', $this->adapter->getId());
    }

    public function testGetCompanyId()
    {
        $this->assertEquals('phonet', $this->adapter->getCompanyId());
    }

    public function testIsIncoming()
    {
        $this->assertFalse($this->adapter->isIncoming());
    }

    public function testIsAnswered()
    {
        $this->assertTrue($this->adapter->isAnswered());
    }

    public function testIsCompleted()
    {
        $this->assertTrue($this->adapter->isCompleted());
    }

    public function testGetStartTime()
    {
        $this->assertEquals(1575992482014, $this->adapter->getStartTime());
    }

    public function testGetDuration()
    {
        $this->assertEquals(208.787, $this->adapter->getDuration());
    }

    public function testGetRecordLink()
    {
        $this->assertIsString($this->adapter->getRecordLink());
        $this->assertEmpty($this->adapter->getRecordLink());
    }

    protected function setUp(): void
    {
        $this->adapter = new HookAdapter(json_decode($this->hook, true));
    }

    protected function tearDown(): void
    {
        $this->adapter = null;
    }
}
