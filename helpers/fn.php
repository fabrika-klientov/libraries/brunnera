<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 09.12.19
 * Time: 19:11
 */

namespace {

    function bAppPath()
    {
        $listPath =  explode('/vendor', __DIR__);
        if (count($listPath) > 1) {
            array_pop($listPath);
            return join('/vendor', $listPath);
        }

        return array_shift($listPath);
    }
}